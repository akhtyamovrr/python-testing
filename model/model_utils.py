from itertools import chain

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import CharField, Value
from django.db.models.functions import Concat

from model.models import Student, Group, Department, Faculty, Teacher, Discipline, Semester, Course, \
    ChosenElectedCourses, ElectiveCourse


class ModelUtils:
    current_semester = None

    # user API
    @staticmethod
    def get_user_by_uuid(uuid):
        try:
            user = ModelUtils.get_student_by_uuid(uuid)
        except ObjectDoesNotExist:
            user = ModelUtils.get_teacher_by_uuid(uuid)
        return user

    @staticmethod
    def get_all_users_id_name_surname():
        info = list(ModelUtils.get_all_students_queryset().values('id', 'name', 'surname'))
        info.extend(list(ModelUtils.get_all_teachers_queryset().values('id', 'name', 'surname')))
        return info

    @staticmethod
    def get_all_users_id_surname_name_filter(substring):
        result = list(
            Student.objects.annotate(
                screen_name=Concat('surname', Value(' '), 'name', output_field=CharField())).filter(
                screen_name__icontains=substring).values('id', 'name', 'surname'))
        return result

    @staticmethod
    def get_all_users_id_name_surname_filter(substring):
        result = list(
            Student.objects.annotate(
                screen_name=Concat('name', Value(' '), 'surname', output_field=CharField())).filter(
                screen_name__icontains=substring).values('id', 'name', 'surname'))
        return result

    # student API
    @staticmethod
    def create_student_without_commit(surname, name, patronymic, email, phone, group, record_book_id):
        return Student(surname=surname, name=name, patronymic=patronymic, email=email,
                       phone=phone, group=group, record_book_id=record_book_id)

    @staticmethod
    def create_student(name, surname, patronymic, email, phone, group, record_book_id):
        student = ModelUtils.create_student_without_commit(name, surname, patronymic, email, phone, group,
                                                           record_book_id)
        student.save()
        return student

    @staticmethod
    def bulk_commit_students(students_list):
        Student.objects.bulk_create(students_list)

    @staticmethod
    def get_student_by_record_book_id(record_book_id):
        return Student.objects.get(record_book_id=record_book_id)

    @staticmethod
    def get_students_by_group(group):
        return Student.objects.all().filter(group=group)

    @staticmethod
    def get_students_by_group_name(group_name):
        group = ModelUtils.get_group_by_name(group_name)
        return ModelUtils.get_students_by_group(group)

    @staticmethod
    def get_student_by_uuid(uuid):
        return Student.objects.get(id=uuid)

    @staticmethod
    def get_all_students_queryset():
        return Student.objects.all()

    # teacher API
    @staticmethod
    def create_teacher_without_commit(surname, name, patronymic, email, phone, login):
        return Teacher(surname=surname, name=name, patronymic=patronymic, email=email,
                       phone=phone, teacher_identifier=login)

    @staticmethod
    def create_teacher(surname, name, patronymic, email, phone, login):
        teacher = ModelUtils.create_teacher_without_commit(surname=surname, name=name, patronymic=patronymic,
                                                           email=email,
                                                           phone=phone, login=login)
        teacher.save()
        return teacher

    @staticmethod
    def get_teacher_by_login(login):
        return Teacher.objects.get(teacher_identifier=login)

    @staticmethod
    def get_teacher_by_uuid(uuid):
        return Teacher.objects.get(id=uuid)

    @staticmethod
    def get_all_teachers_queryset():
        return Teacher.objects.all()

    # group API
    @staticmethod
    def get_group_by_name(group_name):
        return Group.objects.get(name=group_name)

    @staticmethod
    def create_group_without_commit(group_name, department):
        return Group(name=group_name, department=department)

    @staticmethod
    def create_group(group_name, department):
        group = ModelUtils.create_group_without_commit(group_name, department)
        group.save()
        return group

    @staticmethod
    def create_group_with_department_name(group_name, department_name):
        department = ModelUtils.get_department_by_name(department_name)
        group = ModelUtils.create_group(group_name, department)
        return group

    @staticmethod
    def get_all_groups_queryset():
        return Group.objects.all()

    @staticmethod
    def get_all_groups():
        return ModelUtils.get_all_groups_queryset()

    @staticmethod
    def get_all_groups_ids_and_names():
        return list(Group.objects.all().values('id', 'name'))

    @staticmethod
    def get_group_by_student_id(uuid):
        student = ModelUtils.get_student_by_uuid(uuid)
        return student.group.id

    @staticmethod
    def get_all_groups_containing_substring(substring):
        return list(Group.objects.filter(name__icontains=substring).values('id', 'name'))

    # department API
    @staticmethod
    def get_department_by_name(name):
        return Department.objects.get(name=name)

    @staticmethod
    def get_all_departments_queryset():
        return Department.objects.all()

    @staticmethod
    def get_all_departments():
        return ModelUtils.get_all_departments_queryset()

    @staticmethod
    def get_all_departments_ids_and_names():
        return list(ModelUtils.get_all_departments_queryset().values('id', 'name', 'abbreviation'))

    @staticmethod
    def get_department_ids_by_uuid(uuid):
        user = ModelUtils.get_user_by_uuid(uuid)
        if isinstance(user, Student):
            return user.group.department.id
        else:
            return list(user.departments.values('id'))

    @staticmethod
    def get_departments_by_uuid(uuid):
        user = ModelUtils.get_user_by_uuid(uuid)
        if isinstance(user, Student):
            return Department.objects.filter(name=user.group.department.name)
        else:
            return user.departments

    @staticmethod
    def get_all_departments_containing_substring(substring):
        info = list(Department.objects.filter(name__icontains=substring).values('id', 'name', 'abbreviation'))
        info_by_abbreviation = list(
            Department.objects.filter(abbreviation__icontains=substring).values('id', 'name', 'abbreviation'))
        info.extend(info_by_abbreviation)
        return info

    @staticmethod
    def create_department_without_commit(name, abbreviation, faculty):
        return Department(name=name, abbreviation=abbreviation, faculty=faculty)

    @staticmethod
    def create_department(name, abbreviation, faculty):
        department = ModelUtils.create_department_without_commit(name, abbreviation, faculty)
        department.save()
        return department

    @staticmethod
    def create_department_with_faculty_abbreviation(name, abbreviation, faculty_abbreviation):
        faculty = Faculty.objects.get(abbreviation=faculty_abbreviation)
        department = ModelUtils.create_department_without_commit(name, abbreviation, faculty)
        department.save()
        return department

    # discipline API
    @staticmethod
    def create_discipline_without_commit(name, description):
        return Discipline(name=name, description=description)

    @staticmethod
    def create_discipline(name, description=''):
        discipline = ModelUtils.create_discipline_without_commit(name, description)
        discipline.save()
        return discipline

    @staticmethod
    def get_discipline_by_name(name):
        return Discipline.objects.get(name=name)

    @staticmethod
    def create_semester(start_date, end_date=None):
        semester = Semester(start_date=start_date, end_date=end_date)
        semester.save()
        return semester

    # course API
    @staticmethod
    def create_course(discipline, teacher, semester, groups):
        course = Course(discipline=discipline, teacher=teacher, semester=semester)
        course.save()
        course.groups.set(groups)
        course.save()
        return course

    @staticmethod
    def create_elective_courses(discipline1, teacher1, discipline2, teacher2, semester, target_groups):
        course1 = ElectiveCourse(discipline=discipline1, teacher=teacher1, semester=semester,
                                 target_groups=target_groups)
        course2 = ElectiveCourse(discipline=discipline2, teacher=teacher2, semester=semester,
                                 target_groups=target_groups)
        course1.alternative = course2
        course2.alternative = course1
        return ElectiveCourse.objects.bulk_create([course1, course2])

    @staticmethod
    def get_courses_by_group(group):
        return Course.objects.filter(groups__in=[group])

    @staticmethod
    def get_courses_by_group_name(group_name):
        group = ModelUtils.get_group_by_name(group_name)
        return ModelUtils.get_courses_by_group(group)

    @staticmethod
    def get_course_students(course):
        if isinstance(course, Course):
            groups = course.groups.values_list('id', flat=True)
            return Student.objects.filter(group__in=groups)
        elif isinstance(course, ElectiveCourse):
            return ChosenElectedCourses.objects.filter(choice=course)

    @staticmethod
    def get_student_courses_by_record_book_id(record_book_id):
        student = ModelUtils.get_student_by_record_book_id(record_book_id)
        courses = ModelUtils.get_courses_by_group(student.group)
        elected_courses = ChosenElectedCourses.objects.filter(student=student)
        return chain(courses, elected_courses)

    @staticmethod
    def get_elective_courses(student):
        return ElectiveCourse.objects.filter(target_groups=student.group)

    @staticmethod
    def get_elective_courses_by_record_book_id(record_book_id):
        student = ModelUtils.get_student_by_record_book_id(record_book_id)
        return ModelUtils.get_elective_courses(student)

    @staticmethod
    def choose_elective_course(course, student):
        choice = ChosenElectedCourses(student=student, choice=course)
        choice.save()
        return choice
