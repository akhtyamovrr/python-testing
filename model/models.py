import uuid

from django.db import models


class CommonInfo(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50)
    abbreviation = models.CharField(max_length=10)

    class Meta:
        abstract = True

    def __str__(self):
        return self.abbreviation + ' (' + self.name + ')'


class University(CommonInfo):
    description = models.CharField(max_length=1000)

    class Meta:
        db_table = "universities"


class Faculty(CommonInfo):
    university = models.ForeignKey(University)

    class Meta:
        db_table = "faculties"


# Кафедра
class Department(CommonInfo):
    faculty = models.ForeignKey(Faculty)

    class Meta:
        db_table = "departments"


class Group(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=10)
    department = models.ForeignKey(Department)

    class Meta:
        db_table = "groups"

    def __str__(self):
        return self.name


class Person(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)
    patronymic = models.CharField(max_length=30, blank=True)
    email = models.EmailField(blank=True, unique=True, null=True)
    phone = models.CharField(max_length=12, blank=True, unique=True, null=True)

    class Meta:
        abstract = True


class Student(Person):
    group = models.ForeignKey(Group)
    record_book_id = models.CharField(unique=True, null=False, max_length=10)

    class Meta:
        db_table = "students"

    def __str__(self):
        return " ".join([self.surname, self.name, self.patronymic]) + ', ' + str(self.group)


class Teacher(Person):
    departments = models.ManyToManyField(Department)
    teacher_identifier = models.CharField(unique=True, null=False, max_length=10)

    class Meta:
        db_table = "teachers"

    def __str__(self):
        return " ".join([self.surname, self.name, self.patronymic])


class Semester(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    start_date = models.DateTimeField(null=False)
    end_date = models.DateTimeField(null=True)

    class Meta:
        db_table = "semesters"


class Discipline(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=1000, null=True)

    class Meta:
        db_table = "disciplines"

    def __str__(self):
        return self.name


class AbstractCourse(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    discipline = models.ForeignKey(Discipline)
    teacher = models.ForeignKey(Teacher)
    semester = models.ForeignKey(Semester)

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.discipline) + ', ' + str(self.teacher)


class Course(AbstractCourse):
    groups = models.ManyToManyField(Group)

    class Meta:
        db_table = "courses"


class ElectiveCourse(AbstractCourse):
    students = models.ManyToManyField(Student, blank=True, null=True)
    alternative = models.ForeignKey('self')
    target_groups = models.ForeignKey(Group)

    class Meta:
        db_table = "elective_courses"


class ChosenElectedCourses(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    student = models.ForeignKey(Student)
    choice = models.ForeignKey(ElectiveCourse)

    class Meta:
        db_table = "chosen_elected_courses"

    def __str__(self):
        return str(self.choice)
