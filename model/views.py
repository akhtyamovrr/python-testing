from django.http import HttpResponse
from rest_framework import viewsets
from transliterate import translit

from model.model_utils import ModelUtils
from model.models import Teacher, Course, Group
from model.serializers import StudentSerializer, University, UniversitySerializer, Faculty, FacultySerializer, \
    DepartmentSerializer, GroupSerializer, Student, TeacherSerializer, DisciplineSerializer, SemesterSerializer, \
    CourseSerializer, GroupIdNameSerializer, DepartmentIdNameSerializer, PersonSerializer


class UniversitiesViewSet(viewsets.ModelViewSet):
    queryset = University.objects.all()
    serializer_class = UniversitySerializer


class FacultiesViewSet(viewsets.ModelViewSet):
    queryset = Faculty.objects.all()
    serializer_class = FacultySerializer


class DepartmentsViewSet(viewsets.ModelViewSet):
    serializer_class = DepartmentSerializer

    def get_queryset(self):
        uid = self.request.query_params.get('uid')
        if uid is not None:
            queryset = ModelUtils.get_departments_by_uuid(uid)
        else:
            queryset = ModelUtils.get_all_departments().values()
        return queryset


class UsersPrimaryInfoViewSet(viewsets.ModelViewSet):
    serializer_class = PersonSerializer

    def get_queryset(self):
        substring = self.request.query_params.get('contains', None)
        if substring is not None:
            return ModelUtils.get_all_users_id_surname_name_filter(substring)
        return ModelUtils.get_all_users_id_name_surname()

    def update(self, request, *args, **kwargs):
        uid = request.query_params.get('uid')
        user = ModelUtils.get_user_by_uuid(uid)
        try:
            phone = self.request.data['phone']
            if phone is not None:
                user.phone = phone
        except KeyError:
            print("no phone for update")

        try:
            email = self.request.data['email']
            if email is not None:
                user.email = email
        except KeyError:
            print("no e-mail for update")
        user.save()
        return HttpResponse(status=200)


class DepartmentsIdNameViewSet(viewsets.ModelViewSet):
    serializer_class = DepartmentIdNameSerializer

    def get_queryset(self):
        substring = self.request.query_params.get('contains', None)
        if substring is not None:
            return ModelUtils.get_all_departments_containing_substring(substring)
        return ModelUtils.get_all_departments_ids_and_names()


class StudentsViewSet(viewsets.ModelViewSet):
    serializer_class = StudentSerializer

    def get_queryset(self):
        rb_id = self.request.query_params.get('record_book_id', None)
        if rb_id is not None:
            rb_id = translit(rb_id, 'ru')
            return Student.objects.filter(record_book_id__iexact=rb_id)
        course_id = self.request.query_params.get('course_id', None)
        if course_id is not None:
            course = Course.objects.get(id=course_id)
            return ModelUtils.get_course_students(course)
        queryset = Student.objects.all()
        group_name = self.request.query_params.get('group', None)
        uid = self.request.query_params.get('uid', None)
        if group_name is not None:
            group = ModelUtils.get_group_by_name(group_name)
            queryset = queryset.filter(group=group)
        elif uid is not None:
            queryset = queryset.filter(id=uid)
        return queryset


class StudentByRecordBookView(viewsets.ModelViewSet):
    serializer_class = StudentSerializer

    def get_queryset(self):
        rb_id = self.kwargs['record_book_id']
        rb_id = translit(rb_id, 'ru')
        queryset = Student.objects.filter(record_book_id__iexact=rb_id)
        return queryset


class TeachersViewSet(viewsets.ModelViewSet):
    serializer_class = TeacherSerializer

    def get_queryset(self):
        queryset = Teacher.objects.all()
        uid = self.request.query_params.get('uid', None)
        if uid is not None:
            queryset = queryset.filter(id=uid)
        return queryset


class TeacherByIdentifier(viewsets.ModelViewSet):
    serializer_class = TeacherSerializer

    def get_queryset(self):
        teacher_identifier = self.kwargs['teacher_id']
        queryset = Teacher.objects.filter(teacher_identifier=teacher_identifier)
        return queryset


class DisciplinesViewSet(viewsets.ModelViewSet):
    serializer_class = DisciplineSerializer


class SemesterViewSet(viewsets.ModelViewSet):
    serializer_class = SemesterSerializer


class CoursesViewSet(viewsets.ModelViewSet):
    serializer_class = CourseSerializer

    def get_queryset(self):
        queryset = Course.objects.all()
        teacher_uid = self.request.query_params.get('teacher_id', None)
        if teacher_uid is not None:
            teacher = Teacher.objects.get(id=teacher_uid)
            queryset = queryset.filter(teacher=teacher)
        return queryset


class GroupsViewSet(viewsets.ModelViewSet):
    serializer_class = GroupSerializer

    def get_queryset(self):
        queryset = Group.objects.all()
        student_id = self.request.query_params.get('student_id', None)
        if student_id is not None:
            student = ModelUtils.get_student_by_uuid(student_id)
            queryset = queryset.filter(name=student.group.name)
        return queryset


class GroupsIdNameViewSet(viewsets.ModelViewSet):
    serializer_class = GroupIdNameSerializer

    def get_queryset(self):
        substring = self.request.query_params.get('contains', None)
        if substring is not None:
            return ModelUtils.get_all_groups_containing_substring(substring)
        return ModelUtils.get_all_groups_ids_and_names()
