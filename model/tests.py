import math

from django.test import TestCase
from django.utils import timezone

from model.model_utils import ModelUtils
from model.models import University, Faculty, Group, Student


class UniversityStructureCase(TestCase):
    group_name = r'ИУ7-27'
    department_name = 'Программирование ЭВМ...'
    faculty_abbreviation = r'ИУ'
    university_abbreviation = 'МГТУ'
    record_book_id = r'12У017'
    teacher_identifier = 'fai'
    teacher = None
    semester = None
    discipline = None

    # constant for amount of entities creation during test (see prepare_course_data and tests that use it)
    n = 3

    def setUp(self):
        # creating university structure
        bmstu = University(name='МГТУ им. Н.Э. Баумана', abbreviation=self.university_abbreviation)
        bmstu.save()
        faculty = Faculty(name='Информатика и системы управления', abbreviation=self.faculty_abbreviation,
                          university=bmstu)
        faculty.save()
        department = ModelUtils.create_department(name=self.department_name, abbreviation='ИУ7', faculty=faculty)
        department.save()
        group = ModelUtils.create_group(self.group_name, department)

        # creating people
        student = ModelUtils.create_student_without_commit(surname=r'Ахтямов', name='Руслан', patronymic='Ринатович',
                                                           email='a.27@mail.ru',
                                                           phone='79154445581', group=group,
                                                           record_book_id=self.record_book_id)
        student2 = ModelUtils.create_student_without_commit(surname='Иванов', name='Игорь', patronymic='Геннадьевич',
                                                            email='ivan.test@mail.ru',
                                                            phone='79156665582', group=group, record_book_id='12У018')
        ModelUtils.bulk_commit_students([student, student2])

        self.teacher = ModelUtils.create_teacher(surname='Федоров', name='Александр', patronymic='Игоревич',
                                                 email='fai.test@bmstu.ru',
                                                 phone='79156665583', login=self.teacher_identifier)
        self.teacher.departments.add(department)
        self.teacher.save()
        self.discipline = ModelUtils.create_discipline('Программирование')
        self.semester = ModelUtils.create_semester(timezone.now())
        ModelUtils.current_semester = self.semester
        ModelUtils.create_course(self.discipline, self.teacher, self.semester, list([group]))

    def test_get_university(self):
        University.objects.get(abbreviation=self.university_abbreviation)

    def test_get_faculty(self):
        Faculty.objects.get(abbreviation=self.faculty_abbreviation)

    def test_get_department(self):
        ModelUtils.get_department_by_name(self.department_name)

    def test_get_group(self):
        ModelUtils.get_group_by_name(self.group_name)

    def test_get_students_by_group(self):
        self.assertEqual(2, len(Student.objects.all()))

    def test_get_by_record_book_id(self):
        self.assertEqual(ModelUtils.get_student_by_record_book_id(self.record_book_id).name, 'Руслан')

    def test_filter_by_group(self):
        new_group = ModelUtils.create_group_with_department_name('ИУ7-28', self.department_name)
        ModelUtils.create_student(surname='Усманов', name='Федор', patronymic='Алексеевич',
                                  email='usman.test@mail.ru',
                                  phone='79156665588', group=new_group, record_book_id='12У014')
        self.assertEqual(2, len(ModelUtils.get_students_by_group_name(self.group_name)))

    def test_get_teacher_by_login(self):
        ModelUtils.get_teacher_by_login(self.teacher_identifier)

    def test_update_student(self):
        student_to_update = ModelUtils.get_student_by_record_book_id(self.record_book_id)
        pk = student_to_update.id
        new_group = ModelUtils.create_group_with_department_name('ИУ7-28', self.department_name)
        student_to_update.group = new_group
        student_to_update.save()
        self.assertEqual(pk, student_to_update.id)
        self.assertEqual(student_to_update.group.name, 'ИУ7-28')

    def test_update_teacher_departments(self):
        teacher_to_update = ModelUtils.get_teacher_by_login(self.teacher_identifier)
        pk = teacher_to_update.id
        department = ModelUtils.create_department_with_faculty_abbreviation('test_of_update', 'ИУ15',
                                                                            self.faculty_abbreviation)
        teacher_to_update.departments.add(department)
        teacher_to_update.save()
        self.assertEqual(pk, teacher_to_update.id)
        self.assertEqual(2, len(list(teacher_to_update.departments.values())))

    def test_get_all_groups(self):
        ModelUtils.create_group_with_department_name('ИУ5-13', self.department_name)
        ModelUtils.create_group_with_department_name('МТ3-12', self.department_name)
        self.assertEqual(3, len(ModelUtils.get_all_groups()))

    def test_get_all_groups_ids_and_names(self):
        ModelUtils.create_group_with_department_name('ИУ5-13', self.department_name)
        ModelUtils.create_group_with_department_name('МТ3-12', self.department_name)
        self.assertEqual(3, len(ModelUtils.get_all_groups_ids_and_names()))

    def test_get_all_departments(self):
        self.prepare_department_test_data()
        self.assertEqual(3, len(ModelUtils.get_all_departments()))

    def test_get_all_departments_ids_and_names(self):
        self.prepare_department_test_data()
        self.assertEqual(3, len(ModelUtils.get_all_departments_ids_and_names()))

    def prepare_department_test_data(self):
        bmstu = University.objects.get(abbreviation=self.university_abbreviation)
        faculty = Faculty.objects.get(abbreviation=self.faculty_abbreviation)
        faculty_new = Faculty(name='Какой-то факультет', abbreviation='РЛ',
                              university=bmstu)
        faculty_new.save()
        ModelUtils.create_department(name='Тестовая кафедра с прогами', abbreviation='РЛ2', faculty=faculty_new)
        ModelUtils.create_department(name='Защита информации', abbreviation='ИУ8', faculty=faculty)

    def test_get_group_id_by_student_uuid(self):
        student_id = ModelUtils.get_student_by_record_book_id(self.record_book_id).id
        group = ModelUtils.get_group_by_name(self.group_name)
        group_id_from_student = ModelUtils.get_group_by_student_id(student_id)
        self.assertEqual(group.id, group_id_from_student)

    def test_get_user_by_uuid(self):
        teacher_id = ModelUtils.get_teacher_by_login(self.teacher_identifier).id
        user_teacher = ModelUtils.get_user_by_uuid(teacher_id)
        self.assertEqual(user_teacher.surname, 'Федоров')
        student_id = ModelUtils.get_student_by_record_book_id(self.record_book_id).id
        user_student = ModelUtils.get_user_by_uuid(student_id)
        self.assertEqual(user_student.surname, 'Ахтямов')

    def test_get_department_info_by_uuid(self):
        teacher_id = ModelUtils.get_teacher_by_login(self.teacher_identifier).id
        departments = ModelUtils.get_department_ids_by_uuid(teacher_id)
        self.assertEqual(1, len(departments))
        expected_id = ModelUtils.get_department_by_name(self.department_name).id
        student_id = ModelUtils.get_student_by_record_book_id(self.record_book_id).id
        department_id = ModelUtils.get_department_ids_by_uuid(student_id)
        self.assertEqual(expected_id, department_id)

    def test_get_all_users_id_name_surname(self):
        self.assertEqual(3, len(ModelUtils.get_all_users_id_name_surname()))

    def test_get_all_groups_containing_substring(self):
        ModelUtils.create_group_with_department_name('ИУ7-28', self.department_name)
        ModelUtils.create_group_with_department_name('МТ2-15', self.department_name)
        self.assertEqual(2, len(ModelUtils.get_all_groups_containing_substring(r'у7')))

    def test_get_all_departments_containing_substring(self):
        self.prepare_department_test_data()
        self.assertEqual(2, len(ModelUtils.get_all_departments_containing_substring(r'иу')))

    def test_get_all_users_id_name_surname_filter(self):
        self.assertEqual(1, len(ModelUtils.get_all_users_id_surname_name_filter(r'ов')))

    def test_remove_group(self):
        Group.objects.get(name=self.group_name).delete()

    def test_get_courses_by_group(self):
        group = ModelUtils.get_group_by_name(self.group_name)
        self.assertEqual(1, len(ModelUtils.get_courses_by_group(group)))

    def test_get_courses_by_group_name(self):
        self.assertEqual(1, len(ModelUtils.get_courses_by_group_name(self.group_name)))

    def test_get_common_course_students(self):
        (students, courses, _) = self.prepare_course_data()
        students = ModelUtils.get_course_students(courses[0])
        self.assertEqual(len(list(students.values())), len(students))

    def test_get_elective_course_students(self):
        _, _, elective_courses = self.prepare_course_data()
        len_1 = len(list(ModelUtils.get_course_students(elective_courses[0]).values()))
        len_2 = len(list(ModelUtils.get_course_students(elective_courses[1]).values()))
        self.assertEqual(self.n * self.n, len_1 + len_2)

    def test_get_courses_by_group_name_advanced(self):
        self.prepare_course_data()
        self.assertEqual(self.n, len(ModelUtils.get_courses_by_group_name('ИУ7-21')))

    def test_get_student_courses_by_record_book_id(self):
        students, _, _ = self.prepare_course_data()
        self.assertEqual(self.n + 1, len(
            list(ModelUtils.get_student_courses_by_record_book_id(students[0].record_book_id))))

    def prepare_course_data(self):
        n = self.n
        ModelUtils.get_department_by_name(self.department_name)
        abbreviations = ['ИУ7-21', 'ИУ7-22', 'ИУ7-23']

        groups = []
        for i in range(n):
            groups.append(ModelUtils.create_group_with_department_name(abbreviations[i], self.department_name))
        students = []
        discipline1 = ModelUtils.create_discipline('Анализ алгоритмов')
        discipline2 = ModelUtils.create_discipline('Аналитическая геометрия')
        counter = 10
        for i in range(n):
            for j in range(n * n):
                students.append(ModelUtils.create_student(surname='Усманов', name='Федор', patronymic='Алексеевич',
                                                          email=str(counter) + 'usman.test@mail.ru',
                                                          phone='791566655' + str(counter), group=groups[i],
                                                          record_book_id='13У0' + str(counter)))
                counter += 1
        disciplines = [self.discipline, discipline1, discipline2]
        courses = []
        for i in range(n):
            course = ModelUtils.create_course(disciplines[i], self.teacher, self.semester, groups)
            courses.append(course)

        teacher2 = ModelUtils.create_teacher("Тестов", "Тест", "Тестович", "g@g.re", "89159009034", "qwerty")
        elective_courses = ModelUtils.create_elective_courses(discipline1, self.teacher, discipline2, teacher2,
                                                              self.semester, groups[0])
        first_choice_part = math.floor(n * n / 2)
        for i in range(first_choice_part):
            ModelUtils.choose_elective_course(elective_courses[0], students[i])
        for i in range(first_choice_part, n * n):
            ModelUtils.choose_elective_course(elective_courses[1], students[i])
        return students, courses, elective_courses
