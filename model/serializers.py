from rest_framework import serializers

from model.models import *


class UniversitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = University
        fields = ('id', 'name', 'abbreviation', 'description')


class FacultySerializer(serializers.HyperlinkedModelSerializer):
    university = UniversitySerializer(many=False)

    class Meta:
        model = Faculty
        fields = ('id', 'name', 'abbreviation', 'university')


class DepartmentSerializer(serializers.HyperlinkedModelSerializer):
    faculty = FacultySerializer(many=False)

    class Meta:
        model = Department
        fields = ('id', 'name', 'abbreviation', 'faculty')


class DepartmentIdNameSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Department
        fields = ('id', 'name', 'abbreviation')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    department = DepartmentSerializer(many=False)

    class Meta:
        model = Group
        fields = ('id', 'name', 'department')


class GroupIdNameSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name')


class PersonSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=36)
    name = serializers.CharField(max_length=30)
    surname = serializers.CharField(max_length=30)


class UpdatePersonSerializer:
    id = serializers.CharField(max_length=36)
    phone = serializers.CharField
    email = serializers.EmailField


class StudentSerializer(serializers.HyperlinkedModelSerializer):
    group = GroupSerializer(many=False)

    class Meta:
        model = Student
        fields = ('id', 'name', 'surname', 'patronymic', 'email', 'phone', 'group', 'record_book_id')


class TeacherSerializer(serializers.HyperlinkedModelSerializer):
    departments = DepartmentSerializer(many=True)

    class Meta:
        model = Teacher
        fields = ('id', 'name', 'surname', 'patronymic', 'email', 'phone', 'departments', 'teacher_identifier')


class DisciplineSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Discipline
        fields = ('id', 'name', 'description')


class SemesterSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Semester
        fields = ('id', 'start_date', 'end_date')


class CourseSerializer(serializers.HyperlinkedModelSerializer):
    discipline = DisciplineSerializer(many=False)
    semester = SemesterSerializer(many=False)
    teacher = TeacherSerializer(many=False)

    class Meta:
        model = Course
        fields = ('id', 'discipline', 'semester', 'teacher')


class ElectiveCourseSerializer:
    discipline = DisciplineSerializer(many=False)
    semester = SemesterSerializer(many=False)
    teacher = TeacherSerializer(many=False)

    class Meta:
        model = ElectiveCourse
        fields = ('id', 'discipline', 'semester', 'teacher')
