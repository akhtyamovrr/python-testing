"""model_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from model.views import StudentsViewSet, GroupsViewSet, DepartmentsViewSet, StudentByRecordBookView, \
    TeachersViewSet, TeacherByIdentifier, CoursesViewSet, GroupsIdNameViewSet, DepartmentsIdNameViewSet, \
    UsersPrimaryInfoViewSet

students_list = StudentsViewSet.as_view({
    'get': 'list',
})

student_by_rb_id = StudentByRecordBookView.as_view({
    'get': 'list',
})

teachers_list = TeachersViewSet.as_view({
    'get': 'list',
})

teacher_by_identifier = TeacherByIdentifier.as_view({
    'get': 'list'
})

courses_list = CoursesViewSet.as_view({
    'get': 'list'
})

groups_list = GroupsViewSet.as_view({
    'get': 'list'
})

groups_id_name_list = GroupsIdNameViewSet.as_view({
    'get': 'list'
})

department_id_name_list = DepartmentsIdNameViewSet.as_view({
    'get': 'list'
})

departments_list = DepartmentsViewSet.as_view({
    'get': 'list'
})

users_list = UsersPrimaryInfoViewSet.as_view({
    'get': 'list',
    'patch': 'update'
})

urlpatterns = format_suffix_patterns([

    # get student by some param
    url(r'students/(?P<record_book_id>.+)/$', student_by_rb_id, name='StudentByRecordBookView'),

    # get teacher by some param
    url(r'teachers/(?P<teacher_id>.+)', teacher_by_identifier, name='TeacherByIdentifier'),
    url(r'teachers/(?P<uid>.+)', teachers_list, name='TeacherByUid'),

    # courses
    url(r'courses/(?P<teacher_id>).+', courses_list, name='Courses by teacher'),

    # groups
    url(r'groups_primary_info((?P<contains>.+){0,1})', groups_id_name_list, name='Get groups'),
    url(r'groups/(?P<student_id>.+)', groups_list),

    # users (student or teacher)
    url(r'users', users_list),

    # departments
    url(r'departments_primary_info((?P<contains>.+){0,1})', department_id_name_list, name='Get departments'),
    url(r'departments/(?P<uid>.+)', departments_list),

    # get all
    url(r'students', students_list, name='StudentViewSet'),
    url(r'teachers', teachers_list, name='TeachersViewSet'),
    url(r'courses', courses_list, name='Courses List'),
    url(r'groups', groups_list, name='Get groups'),
    url(r'departments', departments_list),

])
